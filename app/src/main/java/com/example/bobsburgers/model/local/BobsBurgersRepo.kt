package com.example.bobsburgers.model.local
import android.content.Context
import com.example.bobsburgers.utils.CharacterGender
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext


class BobsBurgersRepo(context:Context) {

    private val bbservice = BobsburgerService.getInstance()
    private val bobsburgersdao = CharacterDB.getInstance(context).characterDao()

    suspend fun getMales(): List<Character> = withContext(Dispatchers.IO){
        val cachedMales = bobsburgersdao.getAllTypedCharacters(CharacterGender.MALE)
        delay(1000)
        return@withContext cachedMales.ifEmpty {
            val remoteMales = bbservice.getMales()

            val entities: List<Character> = remoteMales.map{
                Character(gender = CharacterGender.MALE, name = it, image = it)
            }
            bobsburgersdao.insertCharacter(*entities.toTypedArray())

            return@ifEmpty entities
        }
    }

}