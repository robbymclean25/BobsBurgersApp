package com.example.bobsburgers.model.local

import androidx.room.PrimaryKey
import androidx.room.Entity
import com.example.bobsburgers.utils.CharacterGender

@Entity
data class Character(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String,
    val image: String,
    val gender: CharacterGender
)
