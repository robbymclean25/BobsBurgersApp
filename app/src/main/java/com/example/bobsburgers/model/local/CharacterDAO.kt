package com.example.bobsburgers.model.local
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.bobsburgers.utils.CharacterGender
@Dao
interface CharacterDAO {
    @Query("SELECT * FROM Character")
    suspend fun getAll() : List<Character>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCharacter(vararg character:Character)

    @Query("SELECT * FROM Character WHERE gender = :characterGender")
    suspend fun getAllTypedCharacters(characterGender: CharacterGender): List<Character>
}