package com.example.bobsburgers.model.local
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.create
interface BobsburgerService {

    companion object{
        private const val BASE_URL = "https://bobsburgers-api.herokuapp.com"
        private const val ENDPOINT = "/characters"


        fun getInstance(): BobsburgerService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()


    }
    @GET(ENDPOINT)
    suspend fun getMales(@Query("count") count: Int = 10): List<String>
}