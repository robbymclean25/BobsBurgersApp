package com.example.bobsburgers.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Character::class],version = 1)
abstract class CharacterDB:RoomDatabase() {
    abstract fun characterDao(): CharacterDAO

    companion object {
        private const val DATABASE_NAME = "character.db"
        private var instance: CharacterDB? = null

        fun getInstance(context: Context): CharacterDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }


        private fun buildDatabase(context: Context): CharacterDB {
            return Room
                .databaseBuilder(context,CharacterDB ::class.java, DATABASE_NAME)
                .build()
        }
    }
}