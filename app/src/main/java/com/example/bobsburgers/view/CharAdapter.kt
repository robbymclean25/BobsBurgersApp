package com.example.bobsburgers.view
import android.view.LayoutInflater
import android.view.ViewGroup
import coil.load
import androidx.recyclerview.widget.RecyclerView
import com.example.bobsburgers.databinding.CharacterItemBinding
import com.example.bobsburgers.model.local.Character

class CharAdapter:RecyclerView.Adapter<CharAdapter.CharViewHolder>() {
    private var list : MutableList<Character> = mutableListOf()
    fun updateList(newList: List<Character>){
        val oldSize = list.size
        list.clear()
        list.addAll(newList)
        notifyItemRangeChanged(0,newList.size)
    }
    class CharViewHolder(val binding: CharacterItemBinding): RecyclerView.ViewHolder(binding.root){
        fun displayImage(character: Character){
            binding.ivCharImage.load(character.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharViewHolder {
       return CharViewHolder(CharacterItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: CharViewHolder, position: Int) {
        holder.displayImage(list[position])
    }

    override fun getItemCount(): Int = list.size



}