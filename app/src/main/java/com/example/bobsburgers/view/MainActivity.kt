package com.example.bobsburgers.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.example.bobsburgers.R
import com.example.bobsburgers.databinding.ActivityMainBinding
import com.example.bobsburgers.model.local.BobsBurgersRepo
import com.example.bobsburgers.viewModel.CharacterVMFactory
import com.example.bobsburgers.viewModel.CharacterViewModel

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    val TAG = "MainActivity"
    lateinit var binding: ActivityMainBinding
    lateinit var vmFactory: CharacterVMFactory
    private val viewModel by viewModels<CharacterViewModel> { vmFactory }
    private val theAdapter: CharAdapter by lazy{
       CharAdapter()
   }
    override fun onCreate(savedInstanceState:Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        vmFactory = CharacterVMFactory(BobsBurgersRepo(this))
        Log.e(TAG,"Welcome to Bob's Burgers!")
        setContentView(binding.root)
        initViews()
        initObservers()
        viewModel.getMales()
    }
    private fun initViews(){
        with(binding.rvMen){
            layoutManager = GridLayoutManager(this@MainActivity,3)
            adapter = theAdapter
        }
    }
    private fun initObservers(){
        viewModel.males.observe(this){state->
            binding.progress.isVisible = state.isLoading
            theAdapter.updateList(state.males)
        }
        binding.btnMenCharacter.setOnClickListener {
            with(binding.rvMen){
                layoutManager = GridLayoutManager(this@MainActivity, 2)
                adapter = theAdapter
            }
        }
        viewModel.males.observe(this) {state->
            theAdapter.updateList(state.males)
        }
    }
}