package com.example.bobsburgers.view

import com.example.bobsburgers.model.local.Character

data class CharacterState(
    val isLoading : Boolean = false,
    val characters: List<Character> = listOf()
)