package com.example.bobsburgers.viewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bobsburgers.model.local.BobsBurgersRepo
class CharacterVMFactory (
    private  val repo: BobsBurgersRepo
    ): ViewModelProvider.NewInstanceFactory(){
            override fun<T: ViewModel> create(modelClass:Class<T>):T{
                    return CharacterViewModel(repo) as T
            }
    }
