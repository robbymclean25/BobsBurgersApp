package com.example.bobsburgers.viewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.bobsburgers.model.local.Character
import androidx.lifecycle.ViewModel
import com.example.bobsburgers.model.local.BobsBurgersRepo
import kotlinx.coroutines.launch

class CharacterViewModel(private val repo: BobsBurgersRepo): ViewModel(){
    private val _males: MutableLiveData<MaleState> = MutableLiveData(MaleState())
    val males: LiveData<MaleState> get() = _males

    fun getMales(){
        viewModelScope.launch {
            _males.value = MaleState(isLoading = true)
            val result = repo.getMales()
            _males.value = MaleState(males = result, isLoading = false)
        }
    }

    data class MaleState(
        val isLoading: Boolean = false,
        val males: List<Character> = emptyList()
    )
}