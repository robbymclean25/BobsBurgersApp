package com.example.bobsburgers.utils

enum class CharacterGender {
    MALE, FEMALE
}